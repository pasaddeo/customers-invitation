package com.gtt.sample.unitTest.util;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import com.gtt.sample.util.ReadStreamUtil;

@RunWith(SpringRunner.class)
public class ReadStreamUtilTest {

	@Test(expected = IOException.class)
	public void readNotExistingResourceWillThrowException() throws IOException {
		Resource res = new ClassPathResource("notExistingFile");
		ReadStreamUtil.readResource(res);
	}

	@Test
	public void readExistingResourceWillReturnString() throws IOException {
		Resource res = new ClassPathResource("customers.json");
		String json = ReadStreamUtil.readResource(res);
		assertNotNull(json);
	}
}
