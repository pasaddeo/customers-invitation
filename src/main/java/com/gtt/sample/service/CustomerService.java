package com.gtt.sample.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gtt.sample.model.Customer;
import com.gtt.sample.model.ResultList;
import com.gtt.sample.util.DistanceFilter;
import com.gtt.sample.util.ReadStreamUtil;

@Service
public class CustomerService {

	private static Logger logger = Logger.getLogger(CustomerService.class);

	@Value("classpath:customers.json")
	private Resource backupRes;

	@Value("${center.latitude}")
	private Double centerLatitude;
	@Value("${center.longitude}")
	private Double centerLongitude;

	private ObjectMapper mapper = new ObjectMapper();

	public ResultList getCustomersFromBackupFile(int kilometers) {
		ResultList result = new ResultList();
		List<Customer> customers = null;
		List<String> errors = new ArrayList<>();
		try {
			customers = getCustomersFromBackupJsonFile();
			logger.info("List of customers read from a json file");
		} catch (IOException e1) {
			logger.error("Problems with json file... trying to get single json formatted lines from the file");
			try {
				customers = getCustomersFromBackupJsonLineFormattedFile();
				errors.add("error-2");
			} catch (IOException e) {
				logger.error("Problems with json file, impossible to get the list, error: " + e.getMessage());
				errors.add("error-1");
			}
		}

		if (customers != null)
			customers = DistanceFilter.filterByDistance(kilometers, centerLatitude, centerLongitude, customers);
		result.setList(customers);
		result.setErrors(errors);

		return result;
	}

	private List<Customer> getCustomersFromBackupJsonFile() throws IOException {
		logger.info("Trying to load info from a json file...");
		String jsonArray = ReadStreamUtil.readResource(backupRes);
		return mapper.readValue(jsonArray, new TypeReference<List<Customer>>() {
		});
	}

	private List<Customer> getCustomersFromBackupJsonLineFormattedFile() throws IOException {
		List<Customer> list = new ArrayList<>();
		List<String> jsonStringList = ReadStreamUtil.readResourceLineByLine(backupRes);
		jsonStringList.forEach(jsonString -> {
			try {
				list.add(mapper.readValue(jsonString, Customer.class));
			} catch (JsonProcessingException e) {
				logger.warn("Line malformed: " + jsonString);
			}
		});
		return list;
	}

}
