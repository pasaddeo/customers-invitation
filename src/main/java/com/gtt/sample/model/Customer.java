package com.gtt.sample.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Customer implements Comparable<Customer> {

	@JsonProperty("user_id")
	private Integer id;
	private String name;
	private String latitude;
	private String longitude;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + "]";
	}

	@Override
	public int compareTo(Customer c) {
		return this.getId().compareTo(c.getId());
	}
}
