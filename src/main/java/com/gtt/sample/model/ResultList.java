package com.gtt.sample.model;

import java.util.List;

public class ResultList {
	private List<Customer> list;
	private List<String> errors;

	public List<Customer> getList() {
		return list;
	}

	public void setList(List<Customer> list) {
		this.list = list;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
}
