package com.gtt.sample;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.gtt.sample.model.Customer;
import com.gtt.sample.model.ResultList;
import com.gtt.sample.service.CustomerService;

@SpringBootApplication
public class CustomersInvitatinApplication implements CommandLineRunner {

	private static Logger logger = Logger.getLogger(CustomersInvitatinApplication.class);

	@Autowired
	private CustomerService customerService;

	public static void main(String[] args) {
		SpringApplication.run(CustomersInvitatinApplication.class, args).close();
	}

	@Override
	public void run(String... args) throws Exception {
		int km = 100;
		if (args.length > 0)
			km = Integer.parseInt(args[0]);

		logger.info("Retrieving the list of customers within " + km + "km of Dublin.");

		ResultList result = customerService.getCustomersFromBackupFile(km);
		Optional<List<Customer>> opt = Optional.ofNullable(result.getList());
		opt.ifPresent(list -> {
			System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			System.out.println("++++++++++++++++++++INVITATION LIST++++++++++++++++++++++");
			System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			Collections.sort(list);
			list.forEach(customer -> System.out.println(customer));
			System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		});
		if (!result.getErrors().isEmpty()) {
			logger.warn("Something went wrong...");
			result.getErrors().forEach(error -> System.out.println(error));
		}
	}

}
