package com.gtt.sample.util;

import java.util.ArrayList;
import java.util.List;

import com.gtt.sample.model.Customer;

public class DistanceFilter {

	static final int RADIUS_OF_EARTH = 6371;

	public static List<Customer> filterByDistance(int maxDistanceInKm, Double centerLatitude, Double centerLongitude,
			List<Customer> customers) {
		if (customers != null) {
			List<Customer> filteredCustomers = new ArrayList<>();
			Double centerLatitudeRadians = Math.toRadians(centerLatitude);
			customers.forEach(customer -> {
				Double latitudeRadians = Math.toRadians(Double.valueOf(customer.getLatitude()));
				Double deltaLongitudeRadians = Math
						.toRadians(Math.abs(Double.valueOf(customer.getLongitude()) - centerLongitude));
				Double centralAngle = Math.acos(
						Math.sin(centerLatitudeRadians) * Math.sin(latitudeRadians) + Math.cos(centerLatitudeRadians)
								* Math.cos(latitudeRadians) * Math.cos(deltaLongitudeRadians));
				Double distance = centralAngle * RADIUS_OF_EARTH;
				if (distance <= maxDistanceInKm)
					filteredCustomers.add(customer);
			});
			return filteredCustomers;
		}
		return null;
	}

}
