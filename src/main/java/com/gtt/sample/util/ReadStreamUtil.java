package com.gtt.sample.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.core.io.Resource;

public class ReadStreamUtil {

	public static String readResource(Resource res) throws IOException {
		StringBuffer sbJson = new StringBuffer();
		Scanner scanner = new Scanner(res.getInputStream());
		while (scanner.hasNext()) {
			sbJson.append(scanner.nextLine());
		}
		return sbJson.toString();
	}

	public static List<String> readResourceLineByLine(Resource res) throws IOException {
		List<String> jsonStringList = new ArrayList<>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(res.getInputStream()));
		while (reader.ready()) {
			String line = reader.readLine();
			jsonStringList.add(line);
		}
		return jsonStringList;
	}
}
