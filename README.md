# Test project "Customers Invitation"

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

The scope is to filter a list of customers (read from a json file) including only any customers within a certain distance from a given point. 


### Tech

The project is developed using Java 8 and Spring Boot.

### Installation

The project requires Java version 8 to run (you will need a JDK in order to make changes and rebuild).

Simply run:

```sh
$ cd customers-invitation
$ java -jar target/customers-invitation-1.0.0-SNAPSHOT.jar
```

Or you can specify the distance - in kilometers - within the people must be to be invited:

```sh
$ java -jar target/customers-invitation-1.0.0-SNAPSHOT.jar 50
```

Run test and rebuild:

```sh
$ ./mvnw package
```

### Output

```sh
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
++++++++++++++++++++INVITATION LIST++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Customer [id=4, name=Ian Kehoe]
Customer [id=5, name=Nora Dempsey]
Customer [id=6, name=Theresa Enright]
Customer [id=8, name=Eoin Ahearn]
Customer [id=11, name=Richard Finnegan]
Customer [id=12, name=Christina McArdle]
Customer [id=13, name=Olive Ahearn]
Customer [id=15, name=Michael Ahearn]
Customer [id=17, name=Patricia Cahill]
Customer [id=23, name=Eoin Gallagher]
Customer [id=24, name=Rose Enright]
Customer [id=26, name=Stephen McArdle]
Customer [id=29, name=Oliver Ahearn]
Customer [id=30, name=Nick Enright]
Customer [id=31, name=Alan Behan]
Customer [id=39, name=Lisa Ahearn]
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
```

### Error code

- `error-1` : file was not correctly formatted
- `error-2` : file was not correctly formatted but the list was read


### Todos

 - Write MORE Tests



